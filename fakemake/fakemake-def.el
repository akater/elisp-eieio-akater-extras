;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'eieio-akater-extras
  authors "Dima Akater" author-email "nuclearspace@gmail.com"
  first-publication-year-as-string "2021"
  package-requires '((emacs "26"))
  org-files-in-order '("eieio-akater-extras")
  site-lisp-config-prefix "20"
  license "GPL-3")

(advice-add 'fakemake-test :before
            (lambda ()
              (org-babel-do-load-languages
               'org-babel-load-languages
               '((lisp . t)))
              (org-babel-lisp-enable-slime-multiple-targets-support)
              (let ((seconds-to-wait 16))
                (require 'slime)
                (message "%s: Starting %s; we'll wait %s seconds for it"
                         'eieio-akater-extras 'slime seconds-to-wait)
                (slime "/usr/bin/sbcl")
                (sleep-for seconds-to-wait))))
